module Tank
  module ActsAsTank
    extend ActiveSupport::Concern

    included do
    end

    module ClassMethods
      def acts_as_tank(options = {})
        cattr_accessor :tank_text_field

        self.tank_text_field = (options[:tank_text_field] || :last_squawk).to_s

        include Tank::ActsAsTank::LocalInstanceMethods
      end
    end

    module LocalInstanceMethods
      def squawk(string)
        write_attribute(self.class.tank_text_field, string.to_squawk)
      end
    end
  end
end

ActiveRecord::Base.send :include, Tank::ActsAsTank
